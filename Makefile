CC = g++
CFLAGS = -Wall -Wextra -Werror -O3 -lgmp -lgmpxx

.SUFFIXES: .cpp .o

PRGS = meet_in_middle rho_pollard

all:\
	$(PRGS)

meet_in_middle: meet_in_middle.o
	$(CC) meet_in_middle.o -lgmp -lgmpxx -o $@

rho_pollard: rho_pollard.o
	$(CC) rho_pollard.o -lgmp -lgmpxx -o $@

%.o:%.cpp
	$(CC) $(CFLAGS) -c $<

clean:
	find . -name '*.o' -exec rm -f {} ';'
	find . -name '*~' -exec rm -f {} ';'
	find . -name '#*#' -exec rm -f {} ';'
	find . -name '.#*' -exec rm -f {} ';'
	find . -name 'core' -exec rm -f {} ';'
	find . -name '*.core' -exec rm -f {} ';'
	-rm -rf $(PRGS)
