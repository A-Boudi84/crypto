#include<gmp.h>
#include<gmpxx.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<inttypes.h>
#include<map>
#include<iterator>
#include<iostream>

using namespace std;

//The goal is to compute the discrete log of h modulo p: h = g^x mod p


//Need a comparer to search in the map
struct mpzCompare
{
    bool operator() (const mpz_t val1, const mpz_t val2) const
    {
        return mpz_cmp(val1, val2) > 0;
    }
};

int main()
{
	int flag;
	uint64_t x0;
	uint64_t x1;
	uint64_t B;
	map<mpz_t,int,mpzCompare> vec;
	map<mpz_t,int>::iterator it;

	mpz_t p;//prime number
	mpz_t g;//generator of Z/pZ
	mpz_t h;//h = g^x for some x < p
	mpz_t tmp;//To lookup in the hash table

	mpz_t inv_g;// 1/g mod p
	mpz_t gB;//g^B mod p
	mpz_t *array;

//Initialization
//-----------------------------------------------------------------------
	mpz_init(p);
	mpz_init(g);
	mpz_init(h);
	mpz_init(tmp);
	mpz_init(inv_g);
	mpz_init(gB);

	B = (1 << 20);//2^20
	array = (mpz_t*)malloc(B*sizeof(mpz_t));//To place mpz_t in the map

	x0=0;
	while(x0 < B)
	{
		mpz_init(array[x0]);
		x0++;
	}
//-----------------------------------------------------------------------

//Must keep the value in flag or the program might Segfault
	flag = mpz_set_str(p,"13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084171",10);

	assert(flag==0);//If flag is not zero then the operation failed

	flag = mpz_set_str(g,"11717829880366207009516117596335367088558084999998952205599979459063929499736583746670572176471460312928594829675428279466566527115212748467589894601965568",10);

	assert(flag==0);

	flag = mpz_set_str(h,"3239475104050450443565264378728065788649097520952449527834792452971981976143292558073856937958553180532878928001494706097394108577585732452307673444020333",10);

	assert(flag==0);

//Build the hash table of h/g^x1
//----------------------------------------------------------------------
//x = x0*B+x1, B= 2^20
//h= g^x=g^(x0*B+x1) <=> h/g^x1=g^(x0*B)

	x1 = 0;
	mpz_set(array[x1],h);//array[0] = h/g^0 mod p = h mod p
	mpz_invert(inv_g,g,p);//inv_g = 1/g mod p
	vec[array[x1]]=0;//array[x1] is in the map
	x1++;

	while(x1 <  B)
	{
		mpz_mul(array[x1],array[x1-1],inv_g);//array[x1] = (h/g^(x1-1))*(1/g)
		mpz_mod(array[x1],array[x1],p);//array[x1] = h/g^x1 mod p
		vec[array[x1]]=x1;//array[x1] is in the map

//		mpz_out_str(stdout,10,array[x1]);
//		cout << vec[array[x1]] << endl;
		x1++;
	}

//Verification
/*
	for(it=vec.begin(); it!=vec.end(); ++it)
		{
			mpz_out_str(stdout,10,it->first);
			cout << endl;
			cout << it->second << endl;
		}
		cout << endl;
*/
//----------------------------------------------------------------------

//Compute a value of g^(x0*B) and look if it appears in the hash table
//----------------------------------------------------------------------

	x0 = 0;
	mpz_pow_ui(gB,g,B);//gB = g^B
	mpz_mod(gB,gB,p);//gB = g^B mod p
	mpz_set_ui(tmp,1);//tmp = g^(x0*B)

	while(x0 < B)
	{
		//if g^(x0*B) belongs in the table, g^(x0*B) = h/g^x1
		if(vec.find(tmp)!=vec.end())//We found the right couple (x0,x1), x = x0*B+x1
		{
			cout << "x0=" << x0 << endl;
			cout << "x1=" << vec.find(tmp)->second << endl;
			printf("x = %lu\n",x0*B+vec.find(tmp)->second);
			break;
		}

		mpz_mul(tmp,tmp,gB);//tmp = g^(x0*B)*g^B = g^((x0+1)*B)
		mpz_mod(tmp,tmp,p);//tmp = g^((x0+1)*B) mod p

		x0++;
	}

//----------------------------------------------------------------------

//Free the memory
//----------------------------------------------------------------------

	x0 = 0;
	while(x0 < B)
	{
		mpz_clear(array[x0]);
		x0++;
	}

	mpz_clear(p);
	mpz_clear(g);
	mpz_clear(h);
	mpz_clear(inv_g);
	mpz_clear(tmp);
	mpz_clear(gB);

	free(array);

//---------------------------------------------------------------------

	return 0;
}
