#include<gmpxx.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>

using namespace std;

//The goal is to factorize a number using Pollard's rho algorithm.

int main()
{
	mpz_class x;
	mpz_class y;
	mpz_class z;
	mpz_class d;
	mpz_class n;

	unsigned int tmp;
	char *str = (char*) malloc(100*sizeof(char));

	cout << "Initial search value: ";
	if(scanf("%u",&tmp) != 1)
	{
		cout << "Error: scanf failed" << endl;
		return 0;
	}

	cout << "Number to factorize: ";
	if(scanf("%s",str) != 1)
	{
		cout << "Error: scanf failed" << endl;
		return 0;
	}

	n.set_str(str,10);
	x = tmp;
	y = tmp;
	d = 1;

	while(d == 1)
	{
		x = (x*x + 1) % n;
		y = (y*y + 1) % n;
		y = (y*y + 1) % n;
		z = (x - y) % n;
		mpz_gcd( d.get_mpz_t(), z.get_mpz_t(), n.get_mpz_t() );
	}

	if( d == n )
		cout << "Factorization failure" << endl;

	else
	{
		cout << "Smallest prime factor: ";
		mpz_out_str(stdout, 10, d.get_mpz_t());
		cout << endl;
	}

	free(str);

	return 0;
}
